/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './components/FormLogin';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);